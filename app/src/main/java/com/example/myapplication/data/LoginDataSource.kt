package com.example.myapplication.data

import android.content.Context
import android.util.Log
import android.widget.Toast
import com.example.myapplication.data.model.LoggedInUser
import com.google.firebase.auth.FirebaseAuth
import java.io.IOException

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
class LoginDataSource {

    companion object{
        var TAG = "LOGINACTIVITY"
        var uid = null
    }

    fun login(email: String, password: String): Result<LoggedInUser> {
        try {
            // TODO: handle loggedInUser authentication
            val uid = FirebaseAuth.getInstance().uid ?: ""
            var auth = FirebaseAuth.getInstance()
            auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener {
                if(!it.isSuccessful)return@addOnCompleteListener
                Log.d(TAG,"Successfully created the user ${it.result?.user?.uid}")
                }

//                .addOnSuccessListener {
//                    if (it.user != null) {
//                        Log.d(TAG,"Successfully signIn ${it?.user?.uid}")
//                        //val user = auth.uid
//                        Log.d(TAG, "signInWithEmail:success")
//
//                        //return Result.Success(fakeUser)
//                    }
//                }.addOnCanceledListener {
//                    Log.w(TAG, "signInWithEmail:failure")
//                }
//            val user = LoggedInUser(java.util.UUID.randomUUID().toString(), "Jane Doe")
            val user = LoggedInUser(java.util.UUID.randomUUID().toString(), "Jane Doe")

            return Result.Success(user)

        } catch (e: Throwable) {
            return Result.Error(IOException("Error logging in", e))
        }
    }

    fun logout() {
        // TODO: revoke authentication
    }
}

