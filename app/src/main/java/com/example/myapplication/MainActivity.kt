package com.example.myapplication

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.Menu
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.example.myapplication.model.User
import com.example.myapplication.ui.chat.SelectNewChatActivity
import com.example.myapplication.ui.login.LoginActivity
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private var isLogged = FirebaseAuth.getInstance().uid

    companion object{
        var TAG = "MainActivity"
        var currentUser : User? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        fetchCurrentUser()

        verifyIsUserLoggedIn()
    }

    private fun fetchCurrentUser(){
        var uid = FirebaseAuth.getInstance().uid
        var ref = FirebaseDatabase.getInstance().getReference("/users/$uid")
        ref.addListenerForSingleValueEvent(object : ValueEventListener{
            override fun onDataChange(p0: DataSnapshot) {
                currentUser = p0.getValue(User::class.java)
                Log.d(TAG, currentUser?.username)
            }
            override fun onCancelled(p0: DatabaseError) {

            }
        })
    }

    private fun verifyIsUserLoggedIn(){

        if(isLogged == null){
            Log.d("MainActivity", "verifyIsUserNotLoggedIn: $isLogged")
            val loginIntent = Intent(this, LoginActivity::class.java)
            loginIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(loginIntent)
        }else{
            Log.d("MainActivity", "verifyIsUserLoggedIn: $isLogged")
            setContentView(R.layout.activity_main)
            val toolbar: Toolbar = findViewById(R.id.toolbar)
            setSupportActionBar(toolbar)

            val fab: FloatingActionButton = findViewById(R.id.fab)
            fab.setOnClickListener {
                val newChatActivity = Intent(this, SelectNewChatActivity::class.java)
                startActivity(newChatActivity)
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                    .setAction("Action", null).show()

            }
            val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
            val navView: NavigationView = findViewById(R.id.nav_view)
            val toggle = ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
            )
            drawerLayout.addDrawerListener(toggle)
            toggle.syncState()

            navView.setNavigationItemSelectedListener(this)
        }
    }

    override fun onBackPressed() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_newChat -> {
                val newChatActivity = Intent(this, SelectNewChatActivity::class.java)
                startActivity(newChatActivity)
            }
            R.id.action_logOut -> {
                FirebaseAuth.getInstance().signOut()
                var intent = Intent(this@MainActivity,LoginActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
            }

        }
        return super.onOptionsItemSelected(item)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        displayView(item.itemId)
        return true
    }

    private fun displayView(itemId: Int) {
        var fragment: Fragment? = null
        var title: String  = getString(R.string.calc_title)

        when (itemId) {
            R.id.nav_home -> {
                fragment = HomeFragment()
                title = getString(R.string.home_title)

            }
            R.id.nav_gallery -> {
                fragment = CalcFragment()
                val fab: FloatingActionButton = findViewById(R.id.fab)
                title = getString(R.string.calc_title)
                fab.hide()
            }
            R.id.nav_chat -> {
                fragment = ChatFragment()
                val fab: FloatingActionButton = findViewById(R.id.fab)
                title = getString(R.string.chat_title)
                fab.hide()
            }
            R.id.nav_tools -> {

            }
            R.id.nav_share -> {

            }
            R.id.nav_send -> {

            }
        }

        if(fragment !== null){
            supportFragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit()
        }
        if(supportActionBar !== null){
            supportActionBar!!.title = title

        }

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        drawerLayout.closeDrawer(GravityCompat.START)
    }

}
