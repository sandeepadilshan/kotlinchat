package com.example.myapplication.ui.chat

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.myapplication.R
import com.example.myapplication.model.User
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import kotlinx.android.synthetic.main.activity_new_chat.*
import kotlinx.android.synthetic.main.user_row_new_message.view.*

class SelectNewChatActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_chat)
        supportActionBar?.title = "Select User"

        fetchUser()
    }

    companion object{
        val USER_KEY = "USER_KEY"
    }

    private fun fetchUser(){
        var ref = FirebaseDatabase.getInstance().getReference("/users")
        ref.addListenerForSingleValueEvent(object : ValueEventListener{
            var adapter = GroupAdapter<GroupieViewHolder>()
            override fun onDataChange(p0: DataSnapshot) {
                p0.children.forEach {
                    Log.d("NewChatActivity",it.toString())
                    var user = it.getValue(User::class.java)
                    if(user != null){
                        adapter.add(UserItem(user))
                    }
                }

                adapter.setOnItemClickListener { item, view ->

                    var userItem = item as UserItem

                    var intent = Intent(view.context, ChatLogActivity::class.java)
                    intent.putExtra(USER_KEY,userItem.user)
                    startActivity(intent)

                }
                recyclerview_newchat.adapter = adapter

            }

            override fun onCancelled(p0: DatabaseError) {

            }
        })
    }
}

class UserItem(val user: User) :Item<GroupieViewHolder>() {

    override fun getLayout(): Int {
        return R.layout.user_row_new_message
    }

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
//      viewHolder.itemView.circle_image_view_userImage.setCircleBackgroundColorResource()
        viewHolder.itemView.text_view_username.text = user.username
        Picasso.get().load(user.slectedPhotoURL).into(viewHolder.itemView.circle_image_view_userImage)
    }
}
