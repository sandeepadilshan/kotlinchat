package com.example.myapplication.ui.signin

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.widget.Button
import android.widget.Toast
import com.example.myapplication.MainActivity
import com.example.myapplication.R
import com.example.myapplication.model.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.activity_sign_in.*
import java.util.*

class SignInActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)
        setTitle("Sign In")
        val signIn = findViewById<Button>(R.id.btnSignIn)
        val back = findViewById<Button>(R.id.btnBack)

        select_button_register.setOnClickListener {
            Log.d("SignInActivity","Select the photo for upload")
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            startActivityForResult(intent,0)
        }

        signIn.setOnClickListener {
            performSignIn()
        }

        back.setOnClickListener {
            val backToMainActivity = Intent(this@SignInActivity,MainActivity::class.java)
            startActivity(backToMainActivity)
        }
    }

    var selectedPhotoUri : Uri? = null

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == 0 && resultCode == Activity.RESULT_OK && data != null){
            // processed and checked what the selected image.....
            Log.d("SignInActivity", "Photo Selected")

            selectedPhotoUri = data.data
            var bitmap = MediaStore.Images.Media.getBitmap(contentResolver,selectedPhotoUri )

            select_imageView_register.setImageBitmap(bitmap)
            select_button_register.alpha = 0f
//            var bitmapDrawable = BitmapDrawable(bitmap)
//            username_editText_register.setBackgroundDrawable(bitmapDrawable)
        }
    }

    private fun performSignIn(){
        var name = name.text.toString()
        var email = email.text.toString()
        var password = password.text.toString()


        Log.d("SignIn", "Name : $name")
        Log.d("SignIn", "Email : $email")

        if(name.isEmpty() || email.isEmpty() ){
            Log.d("SignIn", "Please enter Email and Password ")
        }

        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email,password)
            .addOnCompleteListener {
                if (!it.isSuccessful) return@addOnCompleteListener

                Log.d("SignInActivity","Successfully created the user ${it.result?.user?.uid}")

                uploadImageToFirebaseStorage()

            }.addOnFailureListener {
                Log.d("SignInActivity", "Failed to create user : ${it.message}")
                Toast.makeText(this,"Filed to Create user : ${it.message}",Toast.LENGTH_LONG)
            }


    }

    private fun uploadImageToFirebaseStorage(){
        if (selectedPhotoUri == null){ return}
        var filename = UUID.randomUUID().toString()
        var ref = FirebaseStorage.getInstance().getReference("/images/$filename")
        ref.putFile(selectedPhotoUri!!)
            .addOnSuccessListener {
            Log.d("SignInActivity","Successfully uploaded the image: ${it.metadata?.path}")

                ref.downloadUrl.addOnSuccessListener {
                    Log.d("SignInActivity","File: $it")
                    saveUserOnFirebaseDatabase(it.toString())
                }
        }
            .addOnFailureListener{
                Log.d("SignInActivity", "Image upload failed")
            }
    }

    private fun saveUserOnFirebaseDatabase(profileImageURL: String){
        val uid = FirebaseAuth.getInstance().uid ?: ""
        val ref = FirebaseDatabase.getInstance().getReference("/users/$uid")

        var user = User(uid,name.text.toString(),profileImageURL)
        ref.setValue(user)
            .addOnSuccessListener {
                Log.d("SignInActivity", "User added to database Successfully")
                var intent = Intent(this@SignInActivity,MainActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
            }.addOnFailureListener {
                Log.d("SignInActivity", "User added filed to database")
            }
    }
}


