package com.example.myapplication.ui.chat

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.myapplication.MainActivity
import com.example.myapplication.R
import com.example.myapplication.model.ChatMessage
import com.example.myapplication.model.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import kotlinx.android.synthetic.main.activity_chat_log.*
import kotlinx.android.synthetic.main.chat_from_row.view.*
import kotlinx.android.synthetic.main.chat_to_row.view.*

class ChatLogActivity : AppCompatActivity() {

    companion object{
        var TAG = "ChatLog"
    }

    var adapter = GroupAdapter<GroupieViewHolder>()
    var toUser : User? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_log)

        recyclerView_chat_log.adapter = adapter

        toUser  = intent.getParcelableExtra<User>(SelectNewChatActivity.USER_KEY)
        supportActionBar?.title = toUser?.username

            //fetchChatLog()

        listenForMessages()

        send_message_button.setOnClickListener {
            Log.d(TAG,"Attempt to send message......${textView_message_text.text.toString()}")
            performSendMessage()
        }
    }



    private fun performSendMessage(){
        var fromID = FirebaseAuth.getInstance().uid

        var text = textView_message_text.text.toString()
        var user  = intent.getParcelableExtra<User>(SelectNewChatActivity.USER_KEY)
        var toID = user.uid

        var ref = FirebaseDatabase.getInstance().getReference("/userMessages/$fromID/$toID").push()

        if(fromID == null)return

        var chatMessage = ChatMessage(ref.key!!,text,fromID,toID,System.currentTimeMillis()/1000 )

        ref.setValue(chatMessage)
            .addOnSuccessListener {
                Log.d(TAG,"Saved our chat message..: ${ref.key}")
                textView_message_text.text.clear()
            }
            .addOnFailureListener {
                Log.d(TAG,"Message not saved..: ${ref.key}")
            }
    }


    private fun listenForMessages(){
        var fromID = FirebaseAuth.getInstance().uid
        var toID = toUser?.uid

        var ref = FirebaseDatabase.getInstance().getReference("/userMessages/$fromID/$toID")
        ref.addChildEventListener(object : ChildEventListener{
            override fun onChildAdded(p0: DataSnapshot, p1: String?) {
                var chatMessage = p0.getValue(ChatMessage::class.java)
                if(chatMessage != null){
                    Log.d(TAG,chatMessage?.text)
                    if(chatMessage.fromId == FirebaseAuth.getInstance().uid ){
                        var currentUser = MainActivity.currentUser ?: return
                        adapter.add(ChatFromItem(chatMessage.text,currentUser))
                    }else {
                        adapter.add(ChatToItem(chatMessage.text, toUser!!))
                    }
                }
            }

            override fun onChildChanged(p0: DataSnapshot, p1: String?) {

            }

            override fun onChildRemoved(p0: DataSnapshot) {

            }

            override fun onCancelled(p0: DatabaseError) {

            }

            override fun onChildMoved(p0: DataSnapshot, p1: String?) {

            }

        })
    }
}


class ChatFromItem(val text:String, val user: User):Item<GroupieViewHolder>(){
    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.itemView.textViewChatFrom.text = text
        Picasso.get().load(user.slectedPhotoURL).into(viewHolder.itemView.imageView_chatFrom)
    }

    override fun getLayout(): Int {
        return  R.layout.chat_from_row
    }
}

class ChatToItem(val text : String,val user: User):Item<GroupieViewHolder>(){
    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.itemView.textViewChatTo.text = text
        Picasso.get().load(user.slectedPhotoURL).into(viewHolder.itemView.imageView_chatTo)
    }

    override fun getLayout(): Int {
        return  R.layout.chat_to_row
    }
}
