package com.example.myapplication


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_calc.*
import net.objecthunter.exp4j.ExpressionBuilder

/**
 * A simple [Fragment] subclass.
 */
class CalcFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.fragment_calc, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindCalcUI()
    }

    private fun bindCalcUI() {
        //Number
        tvOne.setOnClickListener { appendOnExpression("1",  true) }
        tvTwo.setOnClickListener { appendOnExpression("2",  true) }
        tvThree.setOnClickListener { appendOnExpression("3",  true) }
        tvFour.setOnClickListener { appendOnExpression("4",  true) }
        tvFive.setOnClickListener { appendOnExpression("5",  true) }
        tvSix.setOnClickListener { appendOnExpression("6",   true) }
        tvSeven.setOnClickListener { appendOnExpression("7",  true) }
        tvEight.setOnClickListener { appendOnExpression("8", true) }
        tvNine.setOnClickListener { appendOnExpression("9 ",  true) }
        tvZero.setOnClickListener { appendOnExpression("0",  true) }
        tvDot.setOnClickListener { appendOnExpression(".",  true) }

        //Operator
        tvPlus.setOnClickListener { appendOnExpression("+" ,   false) }
        tvMinus.setOnClickListener { appendOnExpression("-",  false) }
        tvDiv.setOnClickListener { appendOnExpression("/", false) }
        tvMul.setOnClickListener { appendOnExpression("*",   false) }
        tvBracketLeft.setOnClickListener { appendOnExpression("(",   false) }
        tvBracketRight.setOnClickListener { appendOnExpression(")",  false) }

        tvClear.setOnClickListener {
            tvExpression.text = ""
            tvResult.text = ""
        }

        tvBack.setOnClickListener {
            val string =tvExpression.text.toString()
            if(string.isEmpty()){
                tvExpression.text = string.substring(0,string.length-1)
            }
        }
        tvResult.text = ""

        tvEqual.setOnClickListener {
            try {
                var expression = ExpressionBuilder(tvExpression.text.toString()).build()
                var result = expression.evaluate()
                var longResult = result.toLong()
                if(result == longResult.toDouble()){
                    tvResult.text = longResult.toString()
                }else{
                    tvResult.text = result.toString()
                }
            }catch (e:Exception){
                Log.d("Exception","Message : "+ e.message)
            }
        }
    }

    private fun appendOnExpression(string : String, canClean : Boolean){

        if(tvResult.text.isNotEmpty()){
            tvExpression.text = ""
        }

        if(canClean){
            tvResult.text = ""
            tvExpression.append(string)
        }else{
            tvExpression.append(tvResult.text)
            tvExpression.append(string)
            tvResult.text = ""
        }
    }


}
